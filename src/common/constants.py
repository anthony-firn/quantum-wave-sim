import scipy.constants as const
import math

# Speed of Light in Vacuum (meters per second)
SPEED_OF_LIGHT = const.speed_of_light

# Planck Constant (Joule seconds)
PLANCK_CONSTANT = const.Planck

# Reduced Planck Constant (Joule seconds)
REDUCED_PLANCK_CONSTANT = PLANCK_CONSTANT / (2 * math.pi)

# Electron Mass (kilograms)
ELECTRON_MASS = const.electron_mass

# Elementary Charge (Coulombs)
ELEMENTARY_CHARGE = const.elementary_charge

# Permitivity of Free Space (Farads per meter)
VACUUM_PERMITTIVITY = const.epsilon_0

# Unitless Constants

# Fine-structure Constant
# α = e^2 / (4πε0ħc)
FINE_STRUCTURE_CONSTANT = (ELEMENTARY_CHARGE ** 2) / (4 * math.pi * VACUUM_PERMITTIVITY * REDUCED_PLANCK_CONSTANT * SPEED_OF_LIGHT)

# Rydberg Constant (meters^-1)
# R∞ = α^2 me c / 2h
RYDBERG_CONSTANT = (FINE_STRUCTURE_CONSTANT ** 2) * ELECTRON_MASS * SPEED_OF_LIGHT / (2 * PLANCK_CONSTANT)

# Bohr Magneton (Joules per Tesla)
# μB = eħ / 2me
BOHR_MAGNETON = ELEMENTARY_CHARGE * REDUCED_PLANCK_CONSTANT / (2 * ELECTRON_MASS)

# Compton Wavelength of an Electron (meters)
# λc = h / me c
COMPTON_WAVELENGTH = PLANCK_CONSTANT / (ELECTRON_MASS * SPEED_OF_LIGHT)

# Example: Default simulation parameters
DEFAULT_WAVELENGTH = 500e-9  # 500 nm, typical wavelength for visible light
DEFAULT_GRID_SIZE = 256  # Size of the simulation grid
DEFAULT_TIME_STEP = 1e-15  # Time step for simulations in seconds

# TODO: Add more constants as needed