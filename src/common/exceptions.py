class QuantumWaveSimError(Exception):
    """Base exception class for QuantumWaveSim."""


class InvalidInputError(QuantumWaveSimError):
    """Raised when invalid input or parameters are provided."""


class SimulationError(QuantumWaveSimError):
    """Raised when an error occurs during the simulation process."""


# TODO add more specific exception classes as needed