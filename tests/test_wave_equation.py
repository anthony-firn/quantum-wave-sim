import unittest
import numpy as np
import scipy.constants as const
import math
import pycuda.autoinit
import pycuda.driver as cuda
import pycuda.gpuarray as gpuarray
from src.wave_solver.wave_equation import WaveEquationSolver3D

class TestWaveEquationSolver(unittest.TestCase):
    def setUp(self):
        # Initialize common parameters for testing
        self.grid_size = 64
        self.time_step = 1e-15
        self.wavelength = 500e-9
        self.mass = const.electron_mass
        self.electric_field = lambda x, y, z, t: math.sin(2 * math.pi * const.speed_of_light * t / self.wavelength) * np.array([1, 0, 0])
        self.slit_width = 100e-9
        self.slit_distance = 500e-9
        self.num_time_steps = 100

    def test_wave_initialization(self):
        # Test the initialization of the wave function
        solver = WaveEquationSolver3D(self.grid_size, self.time_step, self.wavelength, self.mass, self.electric_field)
        solver.initialize_wave(self.slit_width, self.slit_distance)
        self.assertTrue(np.any(solver.grid.get()))

    def test_wave_evolution(self):
        # Test the time evolution of the wave function
        solver = WaveEquationSolver3D(self.grid_size, self.time_step, self.wavelength, self.mass, self.electric_field)
        solver.initialize_wave(self.slit_width, self.slit_distance)
        solver.evolve(self.num_time_steps)
        self.assertTrue(np.any(solver.grid.get()))

    def test_wave_intensity_calculation(self):
        # Test the wave intensity calculation
        solver = WaveEquationSolver3D(self.grid_size, self.time_step, self.wavelength, self.mass, self.electric_field)
        solver.initialize_wave(self.slit_width, self.slit_distance)
        solver.evolve(self.num_time_steps)
        intensity = solver.get_wave_intensity()
        self.assertTrue(np.any(intensity.get()))

    def test_wave_phase_calculation(self):
        # Test the wave phase calculation
        solver = WaveEquationSolver3D(self.grid_size, self.time_step, self.wavelength, self.mass, self.electric_field)
        solver.initialize_wave(self.slit_width, self.slit_distance)
        solver.evolve(self.num_time_steps)
        phase = solver.get_wave_phase()
        self.assertTrue(np.any(phase.get()))

if __name__ == "__main__":
    unittest.main()