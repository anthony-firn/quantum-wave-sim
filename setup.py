from setuptools import setup, find_packages

setup(
    name='quantum-wave-sim',
    version='0.1.0',
    author='Anthony Firn',
    author_email='anthonyfirn@illi.ac',
    description='A Blender plugin for simulating and visualizing quantum and electromagnetic phenomena.',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/anthony-firn/quantum-wave-sim/',
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    install_requires=[
        'numpy',
        'scipy',
        'matplotlib',  # TODO Add other dependencies
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        # Replace these appropriately
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    keywords='quantum mechanics simulation blender plugin',  # TODO Add some relevant keywords
    python_requires='>=3.7',
)
